import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Recipe } from '../../models/recipe.interface';
import { FirestoreService } from '../../services/data/firestore.service';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  public recipe: Recipe;
  user = localStorage.getItem("email");
  constructor(
    private firestoreService: FirestoreService,
    private route: ActivatedRoute,
    private alertController: AlertController,
    private router: Router,
    public loadingCtrl: LoadingController,
  ) { }

  ngOnInit() {
    const recipeId: string = this.route.snapshot.paramMap.get('id');
    this.firestoreService.getRecipeDetail(recipeId).subscribe(recipe =>{
      this.recipe = recipe;
    })
  }

  async deleteRecipe(recipeId: string, recipeName: string): Promise<void>{
      const alert = await this.alertController.create({
        message: "Are you sure you want to delete this " + recipeName + " recipe? ",
        buttons:[
          {
            text:'Cancel',
            role:'cancel',
            handler: blah =>{
              console.log('Confirm Cancel: blah');
            },
          },
          {
            text:'Okay',
            handler: () => {
              this.firestoreService.deleteRecipe(this.recipe.id).then(() => {
                  this.router.navigateByUrl('');
              });
            },
          },
        ],
      });
      await alert.present();
  }


//saves the recipe to that specific user
  async save(recipeId: string,recipeName: string,recipeTime: string){
    const loading = await this.loadingCtrl.create();
    const recipe = recipeId;
    const user = localStorage.getItem("email");
    const name = recipeName
    const time = recipeTime
    console.log(user);
    this.firestoreService
    .saveRecipe(recipe,user,name,time).then(
      () => {
              this.router.navigateByUrl('tabs/tab2');;
    },
    error => {
      loading.dismiss().then(() => {
        console.error(error);
      });
    }
);

}
}
