// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAM7SDEyODdDfbYy2IxrYP4ALzsYlGcYf0",
    authDomain: "recipe-pwa-fcbdc.firebaseapp.com",
    databaseURL: "https://recipe-pwa-fcbdc-default-rtdb.firebaseio.com",
    projectId: "recipe-pwa-fcbdc",
    storageBucket: "recipe-pwa-fcbdc.appspot.com",
    messagingSenderId: "269613340376",
    appId: "1:269613340376:web:8428528a04cb67d4c3a9ad",
    measurementId: "G-R29CXCNZV4"
  }
};

var firebaseConfig = {
  apiKey: "AIzaSyAM7SDEyODdDfbYy2IxrYP4ALzsYlGcYf0",
  authDomain: "recipe-pwa-fcbdc.firebaseapp.com",
  databaseURL: "https://recipe-pwa-fcbdc-default-rtdb.firebaseio.com",
  projectId: "recipe-pwa-fcbdc",
  storageBucket: "recipe-pwa-fcbdc.appspot.com",
  messagingSenderId: "269613340376",
  appId: "1:269613340376:web:8428528a04cb67d4c3a9ad",
  measurementId: "G-R29CXCNZV4"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
