import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, RouterModule } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
//firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { firebaseConfig } from './credentials'

import { environment } from '../environments/environment';
//import { from } from 'rxjs';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
            AngularFireModule.initializeApp(environment.firebase),
            AngularFirestoreModule, // imports firebase/firestore, only needed for database features
            AngularFireAuthModule, // imports firebase/auth, only needed for auth features
            AngularFireModule.initializeApp(firebaseConfig), //imports firestoremodule and provides crendtials
            ServiceWorkerModule.register('ngsw-worker.js', {
                                            enabled: environment.production,
                                            // Register the ServiceWorker as soon as the app is stable
                                            // or after 30 seconds (whichever comes first).
                                            registrationStrategy: 'registerWhenStable:30000'
                                          })],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}



