import { Component,OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from "firebase/app";
import { FormGroup, FormBuilder, Validators,ReactiveFormsModule } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import {Router} from '@angular/router'

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  // comment

  user = null;
  public loginForm:FormGroup;
  

  constructor(formBuilder: FormBuilder,
              public fireAuth: AngularFireAuth) {
                this.fireAuth.authState.subscribe((user) => {
                this.user = user ? user : null;
                //adds the users email to the localstorage so we can use it to save recipes
                localStorage.setItem("email",this.user.email);
        });

        this.loginForm = formBuilder.group({
          email:['',Validators.required],
          password:['',Validators.required],
        });
  }


  login(){
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;
    
    
    this.fireAuth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
    
  }
  logout(){
    localStorage.clear
    this.fireAuth.signOut();
  }

}
