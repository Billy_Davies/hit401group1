export interface Recipe {
    id: string;
    recipeName: string;
    region: string;
    timeToCook: string;
    ingredients: string;
    instructions:string;
}